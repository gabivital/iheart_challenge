@register-user
Feature: Register a new user
  This rest API service should let anyone register new users.
  Required parameters: user ID, body and title

  Background:
    Given I have a registered user with ID

  @register-new-user
  Scenario: Register an user using and existing userID
    And I have a body information for that userId
    And I have a title information for that userId
    When I make a post request to register endpoint the required parameters
    Then API status code response should be 201
    And I see the new record generated with all the information

  @register-user-null-parameter-error
  Scenario: Trying to register a new user without title
    But I don't add title information for that userId
    When I make a post request to register endpoint with the information that I have
    Then API status code response should be 422
    And I receive the following business error message:
      | Title | Title must not be empty |

  @register-new-user-missing-parameter
  Scenario: Trying to register a new user without title
    And I have a body information for that userId
    But I don't add title parameter to the request
    When I make a post request to register endpoint with the information that I have
    Then API status code response should be 400
    And I receive the following error message:
      | Title | Title parameter must be declared and must not be empty |

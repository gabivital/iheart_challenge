@ignore
Feature: Cart
  This feature should let customers buy any products announced on the website http://automationpractice.com/index.php.
  After every scenario the client's cart should have 0 items.

  Background:
    Given I visit http://automationpractice.com/index.php
    And I sign in with Gabriela's account

  Scenario: Successfully add an item to cart from home page
    Given the item "Printed Chiffon Dress" is available on the home page
    When I add this product to my cart
    Then a pop-up should be displayed with the following message:
      | Message        | Product successfully added to your shopping cart |
      | Quantity       | 1                                                |
      | Total          | $16.40                                           |
      | Cart Message   | There is 1 item in your cart.                    |
      | Total products | $16.40                                           |
      | Total shipping | $2.00                                            |
      | Total Cart     | $18.40                                           |

  Scenario Outline: Successfully add an item to cart that's in the home page from quick view option
    Given the item "Printed Chiffon Dress" is available on the home page
    And I click on quick view option
    When I add <quantity> unit(ies) of this product to my cart
    Then a pop-up should be displayed with the following message:
      | Message        | Product successfully added to your shopping cart |
      | Quantity       | <quantity>                                       |
      | Total          | <total_price>                                    |
      | Cart Message   | There <quantity> in your cart.                   |
      | Total products | <total_price>                                    |
      | Total shipping | $2.00                                            |
      | Total Cart     | <total_cart>                                     |

    Examples:
      | quantity | total_price | total_cart |
      | 1        | 16.40       | 18.40      |
      | 4        | 65.60       | 73.60      |

  Scenario Outline: Successfully add an item to cart from the product page
    Given I search for "Faded Short Sleeve T-shirts"
    And I visit the searched product page
    When I add <quantity> unit(ies) of this product to my cart
    Then a pop-up should be displayed with the following message:
      | Message        | Product successfully added to your shopping cart |
      | Quantity       | <quantity>                                       |
      | Total          | <total_price>                                    |
      | Cart Message   | There <quantity> in your cart.                   |
      | Total products | <total_price>                                    |
      | Total shipping | $2.00                                            |
      | Total Cart     | <total_cart>                                     |

    Examples:
      | quantity | total_price | total_cart |
      | 1        | 16.51       | 18.51      |
      | 4        | 49.53       | 51.53      |

  Scenario: Successfully add 2 different products to cart from home page
    Given I added "Printed Chiffon Dress" from the home page
    And I added "Faded Short Sleeve T-shirts" from the home page
    When I check the drop down cart button
    Then I see that the products I added are listed

  Scenario: Successfully add another of the same item from shopping cart screen
    Given I added 1 unit of "Printed Chiffon Dress" to cart
    And I am at the check page
    When I add another unit of that product to cart
    Then quantity must reflect the new quantity
    And the total should reflect the added price

  Scenario: Check if item remains in cart after leaving the checkout page
    Given I added 1 unit of "Printed Chiffon Dress" to cart
    And I visit the checkout page
    When I go back to home page
    Then my item should remain listed in the dropdow cart

  Scenario: Check if item remains in cart once I sign out of my account
    Given I have multiple items in my cart
    And I signed out of my account
    When I sign back in with Gabriela's account
    Then my items are listed on the drop down cart at the home page

  Scenario: Check if items remains in cart when logging in an anonymous window
    Given I have multiple items in my cart
    And I signed out of my account
    When I sign back in with Gabriela's account in an anonymous window
    Then my items are listed on the drop down cart at the home page

  Scenario: Check if items remains in cart after logging in/signing out of another account
    Given I have multiple items in my cart
    And I signed out of my account
    And I logged in with Fernanda's account
    When I sign back in with Gabriela's account
    Then my items are listed on the drop down cart at the home page

  Scenario Outline: Completely remove an item from checkout screen
    Given I have a single item in the cart
    And I am at the checkout page
    When I remove the item clicking on <button> button
    Then the item should be removed from cart
    And the Total should be displayed as '$0,00'

    Examples:
      | button            |
      | '-' on Qty column |
      | Trash bin icon    |

  Scenario: Remove a unit of an item from cart screen
    Given I added 2 units of "Printed Chiffon Dress" to cart
    And I visited the checkout page
    When remove one unit of that item
    Then the item quantity should change to 1
    And the Total Price should reflect the subtracted price of the removed item

  Scenario: Remove item from cart from dropdown cart on the home page
    Given I added 1 unit of "Printed Chiffon Dress" to cart
    When I remove it from the drop down cart
    Then the selected items should be removed
    And the total should be reflect '$0,00'
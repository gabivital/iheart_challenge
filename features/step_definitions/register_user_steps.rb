Given('I have a registered user with ID') do
  steps %(
          When I make a request with filter ID "8" to the service
          )

  @user_id = @response['id']
end

Given('I have a body information for that userId') do
  @body = Faker::HarryPotter.spell
end

Given('I have a title information for that userId') do
  @title = Faker::Yoda.quote
end

Given("I don't add title parameter to the request") do
  # This parameter will be removed on the User method register_new_user
  # to validate required parameter message error
  @title = 'nil'
end

Given("I don't add title information for that userId") do
  @title = nil
end

When(/^I make a post request to register endpoint (?:with the information that I have|the required parameters){1}$/) do
  @response = User.new.register_new_user(@user_id, @body, @title)
end

Then('I see the new record generated with all the information') do
  # asserts
  expect(@response['id']).should_not be_nil
  expect(@response['title']).to eql @title
  expect(@response['body']).to eql @body
  expect(@response['userId']).to eql @user_id
end

Then(/^I receive the following(?: business){0,1} error message:$/) do |table|
  # asserts
  error = table.raw[0].to_a
  expect(@response['message']).to eql error[1]
end

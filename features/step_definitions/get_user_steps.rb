Given('there are records of registered users on the database') do
  # There's no implementation needed here since all data should be previously registered.
end

Given(%r{^the get users feature/endpoint is available$}) do
  # There's no implementation needed here since all data should be previously registered.
end

When('I make a request with filter ID {string} to the service') do |user_id|
  # Makes a request to "https://jsonplaceholder.typicode.com/users"
  @response = User.new.get_user(user_id)
end

When('I make a request with no filter to the service') do
  # Makes a request to "https://jsonplaceholder.typicode.com/users"
  @response_list = User.new.get_user('')
end

Then('API status code response should be {int}') do |status_code|
  expect(@response_list.code).to eql status_code if @response.nil?
  expect(@response.code).to eql status_code if @response_list.nil?
end

Then(%r{^a single record of a customer and his/hers information is returned, such as:$}) do |_customer_info_table|
  # Precondition:
  #   User must have name, username and email
  #   User email must be valid
  expect(@response['name']).should_not be_nil
  expect(@response['username']).should_not be_nil
  expect(@response['email']).should_not be_nil
  expect(ValidateEmail.valid?(@response['email'])).to eql true
end

Then("the customer company's catchphrase must have less than {int} characters") do |_catchphrase_size|
  expect(@response['company']['catchPhrase'].length).to be < 50
end

Then('a list of customers and their information is returned, such as:') do |_customer_info_table|
  # Validates user list based on the following precondition:
  #   User must have name, username and email
  #   User email must be valid

  @response_list.each do |document|
    @response = document
    steps %(Then a single record of a customer and his/hers information is returned, such as:
      | name                  |
      | username              |
      | email                 |
      | company's catchphrase |)
  end
end

class User
  include HTTParty

  def initialize
    @url = 'https://jsonplaceholder.typicode.com/'

    # mapping endpoint
    @get_endpoint = 'users/'
    @register_user = 'posts'
  end

  def get_url(base, path)
    URI.join(base, path)
  end

  def get_user(user_id)
    # Concatenates url with user ID
    @get_endpoint = @get_endpoint << user_id

    path = get_url(@url, @get_endpoint)

    $logger.info("HTTP request: #{path}")
    response = HTTParty.get(path)
  end

  def register_new_user(*args)
    path = get_url(@url, @register_user)

    json_body = {
      userId: args [0],
      body: args[1],
      title: args[2]
    }.delete_if { |_key, value| value == 'nil' }.to_json

    puts json_body

    $logger.info("HTTP request to #{path}")
    $logger.info("Request body #{json_body}")
    response = HTTParty.post(path, body: json_body, headers: { 'Content-type' => 'application/json' })
  end
end

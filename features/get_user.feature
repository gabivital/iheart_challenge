@get-user
Feature: Get registered users information
  This feature should be able to return every registered user information, as well as
  specific user information.
  All customers must have on their records their name, a valid email and an username.

  Background:
    Given there are records of registered users on the database
    And the get users feature/endpoint is available

  @get-single-user-record
  Scenario: Get a specific record using API filter parameter
    When I make a request with filter ID "8" to the service
    Then API status code response should be 200
    And a single record of a customer and his/hers information is returned, such as:
      | name                  |
      | username              |
      | email                 |
      | company's catchphrase |
    And the customer company's catchphrase must have less than 50 characters

  @get-all-user-records
  Scenario: Get a list of users and their information
    When I make a request with no filter to the service
    Then API status code response should be 200
    And a list of customers and their information is returned, such as:
      | name                  |
      | username              |
      | email                 |
      | company's catchphrase |
    And the customer company's catchphrase must have less than 50 characters
### Dependencies

* Ruby 2.4.x

## Installation Instructions

#### Install bundler
To get started, install the bundler:

```
  gem install bundler
```

#### Project Installation
Then you should be able to run the following command to prepare your environment:

```
  bundle install
```

### Run on your Machine

The project uses Cucumber to run its tests, you can use the following command to run all tests:
```
  cucumber
```
Please beware that there are features with the @ignore tag. If you run all scenarios cucumber will notify unimplemented steps.

Or you can run a specific feature test or a specific scenario test. Just run the following command:
```
  cucumber --tags @<test-tag>
```

To ignore any tagged scenario you can run the following command:

```
  cucumber --tags "@<test-tag> and not @<test-tag>"
```
  

### TAGS

Available tags on this project:

* Feature:

    - Get User:
        - @get-user (global tag)
        - @get-single-user-record
        - @get-all-user-records
    - Register New User:
        - @register-user (global tag)
        - @register-new-user
        - @register-user-null-parameter-error
        - @register-new-user-missing-parameter